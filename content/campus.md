---
title: "Campus"
date: 2018-09-08T12:37:17-07:00
draft: false
---

Saraha Children’s School resides within the Saraha Nyingma Buddhist
Institute’s 7,000 square foot educational & temple facility and upon
its beautifully forested 2.5 acre campus grounds. During the weekdays,
when school is in session, SCS occupies and utilizes this entire space
as its own.

<div class="columns">

<div class="column">
<p>
For example, for the first class period of the day, the entire student
body assembles “upstairs” in Saraha Temple to meditate, and receive
child-oriented introductory teachings in Tibetan language and lessons
of the Dharma.  Children also enjoy their lunches upstairs.
</p>

<p>
For the rest of the day, during which traditional academic subjects
will be emphasized, classes are divided by project, age and grade, and
will meet in the 2,500 square feet of dedicated classrooms
“downstairs.“
</p>

<p>
Throughout the day, children have supervised access to the School’s
large playground, to the West of the School, and to the natural forest
to its East.
</p>

<p>
For more information on the school’s, and especially the School’s
Integrated Curriculum, click HERE…
</p>

<p>
For a satellite map of the property, including its location in Eugene,
Oregon, see below…
</p>
</div>

<div class="column">
<a href="/img/scs-architecture-image-600.jpg">
  <img src="/img/scs-architecture-image-600.jpg">
</a>
</div>

</div>

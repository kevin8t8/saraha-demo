---
title: "Welcome"
date: 2018-09-06T13:49:00-07:00
draft: false
title: Saraha Children’s School
---

Welcome!  Saraha Children’s School (SCS), grades K-8, is a multiple
award-winning, state-registered non-profit alternative education
private school, employing project-based learning methods and
integrating child-oriented Buddhist teachings of mindfulness, kindness
and compassionate virtue within a full and rigorous academic
curriculum.  (whew)

SCS operates within the charter and on the beautiful, forested grounds
of Saraha Nyingma Buddhist Institute in Eugene, Oregon.  Ours is the
first ODE-registered Buddhist-oriented children’s school in the State
of Oregon, and one of the first such schools in this country.

The school is preparing to begin the 2018-19 academic year.
Enrollment is still available for the Fall term. Our NEWS page can
tell you more. Look through this site to learn more about the
background, philosophy, policies, curriculum, calendar and activities
of Saraha Children School, and more about our Students and Teachers.

<div class="columns">
  <div class="column">
    {{< youtube id="AEHADlB7Ptw">}}
  </div>
  <div class="column">
    Second column
  </div>
</div>

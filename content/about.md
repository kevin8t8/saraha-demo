---
title: "About Saraha Children’s School"
date: 2018-09-08T12:32:36-07:00
draft: false
---

Saraha Children’s School (or SCS) an award-winning, Oregon
State-registered, non-profit private, alternative education school,
employing project-based learning methods and integrating
child-oriented Buddhist teachings of mindfulness, kindness and
compassionate virtue within a full and rigorous academic curriculum.

Families choose SCS for different reasons. Some incline towards the
Buddhist and spiritual aspects of the school. Some favor the beautiful
school environment and campus, small class sizes, opportunities for
private instruction, and project-based teaching methods. Some value
most the experience, personalities and academic qualifications of the
staff.

The essence of the School is a combination of these factors; a
foundation of spiritual ideas and purpose combined with strong,
practical and customized academics working within a beautiful,
harmonious environment of students, families, staff and place.

For Frequently Asked Questions about Saraha Children’s School, click
HERE…

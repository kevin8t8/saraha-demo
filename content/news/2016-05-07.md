---
title: "May. 7, 2016, Saraha Children’s School Wins Multiple Awards at UofO Science Fair"
date: 2018-09-08T15:42:14-07:00
draft: false
---

This year, surpassing its performance from 2015, SCS received two 1st
place awards, for both its grades K-3 & for grades 4-6 projects, and
also one honorable mention (for the kindergarten class, which also
competed in grades K-3) This year’s projects were, for the higher
grades, on the status and effects of invasive species on the forest
ecology of Saraha forest, and the prognosis for change following their
removal; and on “optimal lego car propulsion” for the kinders…  HERE
are some photos from this event…
